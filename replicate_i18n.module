<?php
/**
 * @file
 * Provide a user interface for the Replicate API.
 */

/**
 * Implements hook_menu().
 */
function replicate_i18n_menu() {
  $default_language = language_from_default();
  $items['replicate/%/%/%'] = array(
    'title' => 'Replicate',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('replicate_i18n_confirm', 1, 2, 3),
    'access callback' => 'replicate_i18n_access',
    'access arguments' => array(1, 2, 3),
  );
  $items['block/%bean_delta/replicate_lg'] = array(
    'title' => 'Replicate Block in ...',
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    // 'page callback' => 'replicate_i18n_bean_redirect',
    // 'page arguments' => array(1, $default_language),.
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'replicate_i18n_confirm',
      'bean',
      1,
      $default_language,
    ),
    'access callback' => 'replicate_i18n_access',
    'access arguments' => array('bean', 1, $default_language),
    'type' => MENU_LOCAL_TASK,
  );
  foreach (replicate_i18n_languages() as $lang => $language) {
    $items['node/%node/replicate/' . $lang] = array(
      'title' => 'Replicate in ' . $language->name,
      'page callback' => 'drupal_get_form',
      'page arguments' => array('replicate_i18n_confirm', 0, 1, 3),
      'access callback' => 'replicate_i18n_access',
      'access arguments' => array(0, 1),
      'type' => ($default_language == $lang) ? MENU_DEFAULT_LOCAL_TASK : MENU_LOCAL_TASK,
    );
    // Adds a standard "Replicate Block" tab on bean edit pages.
    // if($default_language == $lang){.
    $items['block/%bean_delta/replicate_lg/' . $lang] = array(
      'title' => 'Replicate in ' . $language->name,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'page callback' => 'replicate_i18n_bean_redirect',
      'page arguments' => array(1, 3),
        // 'page callback' => 'drupal_get_form',
        // 'page arguments' => array('replicate_i18n_confirm', 'bean', 1, 3),.
      'access callback' => 'replicate_i18n_access',
      'access arguments' => array('bean', 1),
      'type' => ($default_language == $lang) ? MENU_DEFAULT_LOCAL_TASK : MENU_LOCAL_TASK,
        // 'type' => MENU_LOCAL_TASK,.
    );
    // }.
  }
  return $items;
}

/**
 * Redirects to the Replicate UI module's entity replication form.
 *
 * @param object $bean
 *   A standard bean object.
 * @param string $language
 *   The language associated with bean.
 */
function replicate_i18n_bean_redirect($bean, $language = '') {
  drupal_goto('replicate/bean/' . $bean->delta . '/' . $language, array('query' => array('destination' => 'admin/content/blocks')));
}

/**
 * Get list installed lnguages.
 *
 * @global type $language
 */
function replicate_i18n_languages() {
  return language_list();
}

/**
 * Access control for access to replicate.
 *
 * @param string $type
 *   Type of entity.
 * @param object $etid
 *   The entity object.
 */
function replicate_i18n_access($type, $etid) {
  if (is_object($etid)) {
    $info = entity_get_info($type);
    $etid = (isset($etid->{$info['entity keys']['id']})) ? $etid->{$info['entity keys']['id']} : '';
  }
  if (!empty($etid)) {
    // Make sure they can create this item and access replicate.
    $entity = entity_load_single($type, $etid);
    // Ensure this exists, they have access to see and create the type
    // and access the ui.
    if (!empty($entity)
      && entity_access('view', $type, $entity)
      && entity_access('create', $type, $entity)
      && user_access('replicate entities')
      ) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Menu callback.  Ask for confirmation of replication.
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @param string $type
 *   Type entity.
 * @param int $entity_id
 *   Id entity.
 * @param string $codelang
 *   Code language.
 */
function replicate_i18n_confirm($form, &$form_state, $type, $entity_id, $codelang = '') {
  // Convert from object to id if we got an object instead.
  if (is_object($entity_id)) {
    $info = entity_get_info($type);
    $entity_id = $entity_id->{$info['entity keys']['id']};
  }
  // Write id / type into form to pass to the submit handler.
  $form['entity_id'] = array(
    '#type' => 'value',
    '#value' => $entity_id,
  );
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['replica_lang'] = array(
    '#type' => 'value',
    '#value' => $codelang,
  );

  global $language;
  $installed_languages = replicate_i18n_languages();
  if (!empty($codelang)
    && in_array($codelang, array_keys($installed_languages))
    && $codelang != $language->language
  ) {
    $namelang = $installed_languages[$codelang]->name;
    return confirm_form($form,
      t('Are you sure you want to replicate %type entity id %id in %lang?', array(
        '%type' => $type,
        '%id' => $entity_id,
        '%lang' => $namelang,
      )),
      '',
      t('This action cannot be undone.'),
      t('Replicate'),
      t('Cancel')
    );
  }
  else {
    return confirm_form($form,
      t('Are you sure you want to replicate %type entity id %id?', array('%type' => $type, '%id' => $entity_id)), '',
      t('This action cannot be undone.'),
      t('Replicate'),
      t('Cancel')
    );
  }
}

/**
 * Execute full book deletion using batch processing.
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 */
function replicate_i18n_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $entity_type = $form_state['values']['entity_type'];
    $entity_type_path = ('taxonomy_term' == $entity_type) ? 'taxonomy/term' : $entity_type;
    $entity_id = $form_state['values']['entity_id'];
    $replica_lang = $form_state['values']['replica_lang'];

    // Load entity for replicate.
    $id = replicate_i18n_entity_by_id($entity_type, $entity_id, $replica_lang);
    $path = $entity_type_path . '/' . $id;

    if ($id) {
      // Redirect to the new item.
      drupal_set_message(t('%type (%id) has been replicated to id %new!', array(
        '%type' => $entity_type,
        '%id' => $entity_id,
        '%new' => $id,
      )));
      if (!isset($_GET['destination'])) {
        drupal_goto($path);
      }
    }
  }
}

/**
 * Replicate the entity corresponding to the type and id passed in argument.
 *
 * @param string $entity_type
 *   The entity type.
 * @param int $id
 *   The unique entity identifier.
 * @param string $replica_lang
 *   The language associated with entity.
 *
 * @return mixed
 *    The newly created entity id if the clone has been created and saved
 *   else FALSE.
 *
 * @see replicate_entity()
 */
function replicate_i18n_entity_by_id($entity_type, $id, $replica_lang) {
  $original = entity_load_single($entity_type, $id);
  if ('node' == $entity_type) {
    $original->original_lang = $original->language;
    $original->replica_lang = $replica_lang;
    return replicate_entity($entity_type, $original);
  }
  elseif ('bean' == $entity_type) {
    $original->original_lang = $original->translations->original;
    $original->replica_lang = $replica_lang;
    return replicate_entity($entity_type, $original);
  }
  else {
  }
}

/**
 * Alter the replica before returning it.
 *
 * This hook is called at the end of the operations
 * of replicate_clone_entity() function, allowing to alter the replicate
 * before it is return to the caller. This function will apply to all
 * replicated entities.
 *
 * @param object $replica
 *   Reference to the fully loaded entity object being saved (the clone) that
 *   can be altered as needed.
 * @param string $entity_type
 *   Type of the entity containing the field.
 * @param object $original
 *   The fully loaded original entity object.
 *
 * @see replicate_clone_entity()
 * @see drupal_alter()
 */
function replicate_i18n_replicate_entity_alter(&$replica, $entity_type, $original) {
  global $user;
  $oldlang = $replica->original_lang;
  $newlang = $replica->language = $replica->replica_lang;

  $installed_languages = replicate_i18n_languages();
  $namelang = $installed_languages[$newlang]->name;
  $title = $replica->title . ' - ' . t(':codelang', array(':codelang' => $namelang));
  $replica->title = $title;
  $replica->uid = $user->uid;

  if ('node' == $entity_type) {
    $replica->language = $newlang;
    if (isset($replica->title_field)) {
      $replica->title_field[$newlang][0]['value'] = $title;
      $replica->title_field[$newlang][0]['format'] = $original->title_field[$oldlang][0]['format'];
      $replica->title_field[$newlang][0]['safe_value'] = $title;
    }

    foreach ($replica as $key => $value) {
      if (('title_field' == $key || 'field_' == substr($key, 0, 6))
        && !in_array(key($value), array($newlang, LANGUAGE_NONE))
        && is_array($value)
        && !empty($value)
      ) {
        if (!isset($value[$newlang])) {
          $replica->$key = array($newlang => $value[$oldlang]);
        }
        if (isset($value[$newlang]) && isset($value[$oldlang])) {
          unset($replica->{$key}[$oldlang]);
        }
      }
      if ('nodehierarchy_menu_links' == $key
        && is_array($value)
        && !empty($value[0])
      ) {
        unset($replica->nodehierarchy_menu_links[0]);
      }
    }
  }
  elseif ('bean' == $entity_type) {
    if ($oldlang != $newlang) {
      $replica->label = $title;
      $field_title = (isset($oldlang)) ? $replica->field_title[$oldlang][0]['value'] . ' - ' . t(':codelang', array(':codelang' => $namelang)) : $replica->field_title[LANGUAGE_NONE][0]['value'] . ' - ' . t(':codelang', array(':codelang' => $namelang));
      $replica->field_title[$newlang] = (isset($oldlang)) ?
      $replica->field_title[$oldlang] : $replica->field_title[LANGUAGE_NONE];
      $replica->field_title[$newlang][0]['value'] = $field_title;
      $replica->field_title[$newlang][0]['safe_value'] = $field_title;
      $replica->translations->original = $newlang;
    }
  }
}

/**
 * Manage the replication of a specific field type.
 *
 * May be used to manage the replication of custom field type,
 * for example node references.
 *
 * @param object $replica
 *   Reference to the fully loaded entity object being saved (the clone) that
 *   can be altered as needed.
 * @param string $entity_type
 *   Type of the entity containing the field.
 * @param string $field_name
 *   Name of the field that is going to be processed.
 *
 * @see replicate_clone_entity()
 */
function replicate_i18n_replicate_field_entityreference(&$replica, $entity_type, $field_name) {
  if ($replica->replica_lang != $replica->original_lang) {
    unset($replica->$field_name);
  }
}

/**
 * Manage the replication of a specific field paragraphs.
 *
 * @param object $replica
 *   Reference to the fully loaded entity object being saved (the clone) that.
 * @param string $entity_type
 *   Type of the entity containing the field.
 * @param string $field_name
 *   Name of the field that is going to be processed.
 */
function replicate_i18n_replicate_field_paragraphs(&$replica, $entity_type, $field_name) {
  if ($replica->replica_lang != $replica->original_lang) {
    unset($replica->$field_name);
  }
}

/**
 * Manage the replication of a specific field atom reference.
 *
 * @param object $replica
 *   Reference to the fully loaded entity object being saved (the clone) that.
 * @param string $entity_type
 *   Type of the entity containing the field.
 * @param string $field_name
 *   Name of the field that is going to be processed.
 */
function replicate_i18n_replicate_field_atom_reference(&$replica, $entity_type, $field_name) {
  if ($replica->replica_lang != $replica->original_lang
    && !in_array($field_name, array('field_scaldimage', 'field_scaldimage_teaser'))
  ) {
    unset($replica->$field_name);
  }
}

/**
 * Implements hook_admin_paths().
 */
function replicate_i18n_admin_paths() {
  $paths = array(
    'replicate/*/*/*' => TRUE,
    'node/*/replicate/*' => TRUE,
  );
  return $paths;
}
